__author__ = 'shikun'
# -*- coding: utf-8 -*-
#
# 截图web
#
from selenium import webdriver
import time

# 注意到，上面的代码中，我并没有在打开页面后立即截图，而是先在页面上执行了一段 JavaScript 脚本，
# 先将页面的滚动条拖到最下方，再拖回顶部，然后才截图。这样的好处是如果页面下方有一些延迟加载的内容，在这个操作之后一般也都已加载了。
def capture(url, save_fn="capture.png"):
  browser = webdriver.Firefox() # Get local session of firefox
  browser.set_window_size(1200, 900)
  browser.get(url) # Load page
  browser.execute_script("""
    (function () {
      var y = 0;
      var step = 100;
      window.scroll(0, 0);

      function f() {
        if (y < document.body.scrollHeight) {
          y += step;
          window.scroll(0, y);
          setTimeout(f, 50);
        } else {
          window.scroll(0, 0);
          document.title += "scroll-done";
        }
      }

      setTimeout(f, 1000);
    })();
  """)

  for i in range(30):
    if "scroll-done" in browser.title:
      break
    time.sleep(1)

  browser.save_screenshot(save_fn)
  browser.close()


if __name__ == "__main__":

  capture("http://www.jb51.net")