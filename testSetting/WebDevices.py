__author__ = 'shikun'
# -*- coding:utf-8 -*-
import configparser

from testMode.BaseWebDevices import *


def getWebDevices():
    config = configparser.ConfigParser()
    config.read('d:\WebDevices.ini')

    server_url = config['DEFAULT']['server_url']
    platform = config["DEFAULT"]['platform']
    version = config['DEFAULT']['version']
    browser = config['DEFAULT']['browser']

    gw = getDevices()
    gw.set_server_url(server_url)
    gw.set_browser(browser)
    gw.set_platform(platform)
    gw.set_version(version)
    return gw
